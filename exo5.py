def somme_pair (liste) :
    """[récupère les nombre pair et les additionne]

    Args:
        liste ([list]): [une liste de nombre]

    Returns:
        [int]: [retourne la liste des nombres pair]
    """
    res = 0
    for nb in liste :
        if nb%2==0:
            res = res + nb
    return res
print (somme_pair([2,4,5,7,8,10]))

def test_somme_pair ():
    assert somme_pair([2,4,5,7,8,10]) == 24
    assert somme_pair([2,4,5,7,10]) == 16
    assert somme_pair([7,8,10]) == 18
    assert somme_pair([]) == 0

def dern_voyelle (phrase) :
    """[Récupère les voyelle de la phrase afin de garder la dernière]

    Args:
        phrase ([str]): [Une phrase]

    Returns:
        [str]: [La derniere voyelle de la phrase]
    """   
    res = None 
    for lettre in phrase :
        if res == None:
            if lettre in 'aeiouy' : 
              res = lettre
        else :
            if lettre in 'aeiouy' : 
              res = lettre
    return res
print (dern_voyelle("JJ'aime le poire"))
def test_dern_voyelle () :
    assert dern_voyelle("Je suis une légende") == 'e'
    assert dern_voyelle("J'aime le poire") == 'e'
    assert dern_voyelle("L'école c cool") == 'o'
    assert dern_voyelle("") == None

def prop_neg (liste) : 
    """[fait le ratio entre les nombre negatif et le total de nb dans la liste]

    Args:
        liste ([list]): [une liste de nb]

    Returns:
        [float]: [le ratio des nb neg et le total de nb de la lsite]
    """    
    res = None
    cpt = 0
    for nb in liste :
        if res == None :
            if nb < 0: 
               res = 1
        else :
            if nb < 0 :
                res +=1
        cpt += 1
    if res != None :
        res = res / cpt 
    return res 
print (prop_neg([-12,6,-2,6,-7,5]))

def test_prop_neg () :
    assert prop_neg([-12,6,-2,6,-7,5]) == 0.5
    assert prop_neg([-12,6,2,3,5]) == 0.2
    assert prop_neg([]) == None



